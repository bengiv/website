import {FC, ReactNode} from "react";

const DocWrapper: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <>
      <article
        className="prose mt-5 lg:mt-8 lg:!mt-0 lg:px-60 w-full max-w-full dark:prose-invert [&_table]:block [&_table]:max-w-full [&_table]:overflow-x-scroll [&_pre]:!bg-ctp-mantle">
        {children}
      </article>
    </>
  );
};

export default DocWrapper;
