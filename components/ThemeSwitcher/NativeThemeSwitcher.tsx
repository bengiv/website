import { faMoon, faSun } from "@fortawesome/free-regular-svg-icons";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTranslation } from "next-i18next";
import { useTheme } from "next-themes";
import { useEffect, useState } from "react";

const NativeThemeSwitcher = () => {
  const { theme, resolvedTheme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);
  const { t: common } = useTranslation("common");

  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    <div className="relative flex items-center gap-2 rounded-lg bg-ctp-surface0 p-2 px-4 font-semibold capitalize text-ctp-text shadow-sm">
      <FontAwesomeIcon icon={resolvedTheme === "light" ? faSun : faMoon} />
      {theme}
      <FontAwesomeIcon icon={faAngleDown} size="sm" />

      {mounted && (
        <>
          <select
            onChange={(e) => {
              setTheme(e.currentTarget.value);
            }}
            className="absolute inset-0 h-full w-full appearance-none opacity-0"
            value={theme}
          >
            <option value="light">{common("theme.light")}</option>
            <option value="dark">{common("theme.dark")}</option>
            <option value="system">{common("theme.system")}</option>
          </select>
        </>
      )}
    </div>
  );
};

export default NativeThemeSwitcher;
