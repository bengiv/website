import { faMoon, faSun } from "@fortawesome/free-regular-svg-icons";
import { faDisplay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTranslation } from "next-i18next";
import { useTheme } from "next-themes";
import { useEffect, useRef, useState } from "react";

const IconThemeSwitcher = () => {
  const [toggled, setToggled] = useState(false);
  const { theme, resolvedTheme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  const { t: common } = useTranslation("common");

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) {
    return (
      <div className="mr-2 hidden items-center rounded-lg p-2.5 text-center text-sm font-medium text-ctp-subtext1 hover:bg-ctp-mantle focus:outline-none focus:ring-4 md:inline-flex">
        <FontAwesomeIcon
          size="lg"
          icon={resolvedTheme === "light" ? faSun : faMoon}
          fixedWidth
        />
      </div>
    );
  }

  const changeTheme = (theme: string) => {
    setTheme(theme);
    setToggled(false);
  };

  return (
    <>
      <button
        className={`${
          resolvedTheme !== "system" && theme !== "system"
            ? "text-ctp-mauve"
            : "text-ctp-subtext1"
        } mr-2 hidden items-center rounded-lg p-2.5 text-center text-sm font-medium hover:bg-ctp-mantle focus:outline-none focus:ring-4 md:inline-flex`}
        type="button"
        onClick={() => {
          setToggled(!toggled);
        }}
        onBlur={() => {
          requestAnimationFrame(() => {
            if (!ref.current?.contains(document.activeElement)) {
              setToggled(false);
            }
          });
        }}
        aria-label="Change Theme"
      >
        <FontAwesomeIcon
          size="lg"
          icon={resolvedTheme === "light" ? faSun : faMoon}
          fixedWidth
        />
      </button>
      <div
        className={`${
          !toggled ? "hidden " : ""
        }z-10 absolute mt-12 w-fit rounded bg-ctp-surface0 font-semibold shadow shadow-lg`}
        ref={ref}
        onBlur={(e) => {
          const currentTarget = e.currentTarget;

          requestAnimationFrame(() => {
            // Check if the new focused element is a child of the original container
            if (!currentTarget.contains(document.activeElement)) {
              setToggled(false);
            }
          });
        }}
      >
        <ul
          className="py-1 text-sm text-ctp-text"
          aria-labelledby="dropdownDefault"
        >
          <li>
            <button
              className={`${
                theme === "light" ? "text-ctp-mauve" : ""
              } flex w-full items-center gap-2 py-2 px-4 hover:bg-ctp-surface1`}
              onClick={() => changeTheme("light")}
            >
              <FontAwesomeIcon size="lg" icon={faSun} />
              {common("theme.light")}
            </button>
          </li>
          <li>
            <button
              className={`${
                theme === "dark" ? "text-ctp-mauve" : ""
              } flex w-full items-center gap-2 py-2 px-4 hover:bg-ctp-surface1`}
              onClick={() => changeTheme("dark")}
            >
              <FontAwesomeIcon size="lg" icon={faMoon} />
              {common("theme.dark")}
            </button>
          </li>
          <li>
            <button
              className={`${
                theme === "system" ? "text-ctp-mauve" : ""
              } flex w-full items-center gap-2 py-2 px-4 hover:bg-ctp-surface1`}
              onClick={() => changeTheme("system")}
            >
              <FontAwesomeIcon size="lg" icon={faDisplay} />
              {common("theme.system")}
            </button>
          </li>
        </ul>
      </div>
    </>
  );
};

export default IconThemeSwitcher;
