import { FC } from "react";
import Image from "next/future/image";

const DocsImage: FC<{
  src: string;
  alt: string;
  width: string | number;
  height: string | number;
}> = ({ src, alt, width, height }) => {
  return <Image alt={alt} src={`/site/${src}`} width={width} height={height} />;
};

export default DocsImage;
