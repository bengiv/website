import type { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
  webServer: {
    command: process.env.CI ? "yarn start" : "yarn dev",
    port: 3000,

    timeout: 120 * 1000,
    reuseExistingServer: true,
  },
  use: {
    baseURL: "http://localhost:3000/site",
  },
  reporter: process.env.CI && [["junit", { outputFile: "results.xml" }]],
};

export default config;
