import { expect, test } from "@playwright/test";

test("test", async ({ page }) => {
  await page.goto("http://localhost:3000/site");

  // Test language switch
  await page.getByRole("button").nth(1).click();

  await page.getByRole("button", { name: "Deutsch (German)" }).click();
  await expect(page).toHaveURL("http://localhost:3000/site/de");

  // Test return to english
  await page.getByRole("button").nth(1).click();

  await page.getByRole("button", { name: "English (Englisch)" }).click();
  await expect(page).toHaveURL("http://localhost:3000/site");
});
