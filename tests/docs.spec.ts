import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
	await page.goto('http://localhost:3000/site');

	// Test homescreen docs link
  await page.getByRole('link', { name: 'Getting Started' }).click();
  await expect(page).toHaveURL('http://localhost:3000/site/docs/crystal-linux/getting-started');

	// Test links on headings
	await page.getByRole('link', { name: 'Installing Crystal Linux' }).nth(1).click();
  await expect(page).toHaveURL('http://localhost:3000/site/docs/crystal-linux/getting-started#installing-crystal-linux');

	// Test sidebar
  await page.getByRole('link', { name: 'Getting Started' }).nth(1).click();
  await expect(page).toHaveURL('http://localhost:3000/site/docs/amethyst/getting-started');

	// Test directory landing
  await page.goto('http://localhost:3000/site/docs/crystal');

	// Test directory landing navigation
  await page.locator('li:has-text("Crystal Linux")').getByRole('link', { name: 'Crystal Linux' }).click();
  await expect(page).toHaveURL('http://localhost:3000/site/docs/crystal/crystal-linux');
});
