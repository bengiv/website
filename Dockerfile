FROM node:21-alpine3.18

WORKDIR /app

COPY . .

WORKDIR /app

RUN yarn add sharp
RUN yarn install
RUN yarn build

EXPOSE 3000

CMD ["yarn", "start", "-p", "3000"]
