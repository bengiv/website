/* eslint-disable react/no-unknown-property */
/* eslint-disable import/no-anonymous-default-export */
import { ImageResponse } from "@vercel/og";
import { NextRequest } from "next/server";

export const config = {
  runtime: "experimental-edge",
};

const font = fetch(
  new URL("../../assets/fonts/AH-Bold.ttf", import.meta.url)
).then((res) => res.arrayBuffer());

export default async function (req: NextRequest) {
  try {
    const { searchParams } = new URL(req.url);

    // ?title=<title>
    const hasTitle = searchParams.has("title");
    const title = hasTitle ? searchParams.get("title")?.slice(0, 100) : null;

    const fontData = await font;

    return new ImageResponse(
      (
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            fontFamily: "AH",
            backgroundColor: "#1e1e2e",
          }}
        >
          <div tw="flex items-center">
            <span className="h-4 w-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 90 127"
              >
                <path
                  fill="#8839ef"
                  d="M26.4779 127L90 63.5L62.5898 36.1528L53.826 44.8965L72.4102 63.5L8.82597 127H26.4779Z"
                />
                <path
                  fill="#8839ef"
                  d="M63.5221 0L0 63.5L27.2859 90.7231H27.4102L36.1119 82.0415L17.5898 63.5L80.9876 0H63.5221Z"
                />
              </svg>
            </span>

            {!title && (
              <h2 tw="text-[#cdd6f4] font-semibold text-6xl ml-6">
                Crystal Linux
              </h2>
            )}
          </div>

          {title && (
            <h2 tw="text-[#cdd6f4] font-semibold text-7xl mt-10 text-center px-4">
              {title}
            </h2>
          )}
        </div>
      ),
      {
        width: 1200,
        height: 630,
        fonts: [
          {
            name: "AH",
            data: fontData,
            style: "normal",
          },
        ],
      }
    );
  } catch (e: any) {
    console.log(`${e.message}`);
    return new Response(`Failed to generate the image`, {
      status: 500,
    });
  }
}
