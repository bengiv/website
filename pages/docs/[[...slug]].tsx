import {serialize} from "next-mdx-remote/serialize";
import {MDXRemote, MDXRemoteSerializeResult} from "next-mdx-remote";
import {ReactElement, useEffect} from "react";
import {resolve} from "path";
import {GetStaticPaths, GetStaticPathsResult, GetStaticProps} from "next";
import {removeExt, walkFiles} from "../../lib/files";
import remarkGfm from "remark-gfm";
import TreeNode from "../../components/TreeItem";
import DocWrapper from "../../components/DocWrapper";
import {findCurrentDir, ITreeItem} from "../../lib/tree";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeSlug from "rehype-slug";
import rehypeHighlight from "rehype-highlight";
import rehypeImgSize from "rehype-img-size";
import {NextPageWithLayout} from "../_app";
import {useRouter} from "next/router";
import Link from "next/link";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import TranslationInfo from "../../components/TranslationInfo";
import Edit from "../../components/Edit";
import {existsSync} from "fs";
import getTree from "../../lib/trees";
import {NextSeo} from "next-seo";
import Image from "next/future/image";
import DocsImage from "../../components/DocsImage";
import rehypeExtractHeadings from "../../lib/extractHeadings";

export const getStaticPaths: GetStaticPaths = async ({ locales }) => {
  const paths: GetStaticPathsResult["paths"] = [];

  for (const locale of locales!) {
    if (existsSync(`_docs/${locale}`)) {
      for await (const file of walkFiles(`_docs/${locale}`)) {
        const path = file.slice(
          resolve(process.cwd(), `_docs/${locale}/`).length
        );

        paths.push({
          params: {
            slug: removeExt(path).split("/").slice(1),
            locale,
          },
        });
      }
    }
  }

  return {
    paths,
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
  const slug = params!.slug === undefined ? [] : (params!.slug as string[]);
  const translations = await serverSideTranslations(locale!, [
    "common",
    "footer",
    "navbar",
    "meta",
  ]);

  const rootySlug = ["root", ...slug];

  const tree = getTree(locale!).copy();
  tree.walkCurrents(rootySlug);

  const me = tree.find(rootySlug);

  if (slug.length === 0 || me === undefined) {
    const plain = tree.plain();
    return {
      props: {
        source: null,
        tree: plain,
        dir: findCurrentDir(plain),
        ...translations,
      },
    };
  }

  const headings: any[] = [];

  const source = await serialize(me.contents!, {
    parseFrontmatter: true,
    mdxOptions: {
      remarkPlugins: [remarkGfm],
      rehypePlugins: [
        rehypeSlug,
        [
          rehypeExtractHeadings,
          {
            headings,
          },
        ],
        [
          rehypeAutolinkHeadings,
          {
            behavior: "wrap",
          },
        ],
        // @ts-ignore
        [rehypeImgSize, { dir: "public" }],
        rehypeHighlight,
      ],
    },
  });

  return {
    props: {
      source,
      tree: tree.plain(),
      headings,
      ...translations,
    },
  };
};

const DocPage: NextPageWithLayout<{
  source: MDXRemoteSerializeResult | null;
  tree: ITreeItem;
  dir: ITreeItem | null;
  headings: { title: string; id: string; rank: number }[];
}> = ({ source, tree, dir, headings }) => {
  const {
    query: { slug },
    push,
  } = useRouter();

  useEffect(() => {
    if (slug === undefined) {
      push("/docs/crystal-linux/getting-started");
    }
  }, [push, slug]);

  return (
    <div className="mx-auto min-h-screen max-w-8xl px-4 pb-4 pt-24 lg:px-8 lg:flex">
      <NextSeo
        title={dir ? dir.pretty! : source!.frontmatter?.title}
        openGraph={{
          images: [
            {
              url: `https://getcryst.al/site/api/og?title=${
                dir ? dir.pretty! : source!.frontmatter?.title
              }`,
              width: 1200,
              height: 630,
              alt: dir ? dir.pretty! : source!.frontmatter?.title,
              type: "image/svg",
            },
          ],
          type: "article",
        }}
      />

      <div
        className="lg:fixed flex inset-0 top-24 left-[max(0px,calc(50%-45rem))] lg:pb-8 right-auto lg:w-64 overflow-y-auto px-2 lg:pl-8">
        <nav className="mb-2 flex flex-col break-normal w-full lg:w-64">
          <TreeNode node={tree} path="/docs"/>
        </nav>
      </div>

      {source && (
        <div
          className="lg:fixed flex inset-0 top-24 right-[max(0px,calc(50%-45rem))] lg:pb-8 left-auto lg:w-64 overflow-y-auto px-2 lg:pr-8">
          <aside
            className="mt-4 flex flex-col gap-1 break-normal border-t border-ctp-surface0 pt-2 w-full text-ctp-text lg:mt-0 lg:w-64 lg:border-none lg:pt-0">
            <p className="font-bold">On this page</p>

            {headings.map(({id, title, rank}) => (
              <Link href={`#${id}`} key={id}>
                <a
                  className={`text-ctp-subtext0 hover:text-ctp-subtext1 ${
                    rank === 2 ? "" : rank === 3 ? "ml-4" : "ml-6"
                  }`}
                >
                  {title}
                </a>
              </Link>
            ))}
          </aside>
        </div>
      )}

      <DocWrapper>
        {dir ? (
          <>
            {dir.pretty !== null && <h1>{dir.pretty}</h1>}
            <TranslationInfo />
            <ul>
              {slug &&
                dir.children.map((child) => (
                  <li key={child.value}>
                    <Link
                      href={`${(slug as string[]).join("/")}/${child.value}`}
                    >
                      <a>{child.pretty ? child.pretty : child.value}</a>
                    </Link>
                  </li>
                ))}
            </ul>
          </>
        ) : (
          <>
            {source!.frontmatter?.title && <h1>{source!.frontmatter.title}</h1>}
            <TranslationInfo />
            <MDXRemote
              {...source!}
              components={{
                Image,
                img: ({ height, width, alt, src }) => (
                  <DocsImage
                    width={width!}
                    alt={alt!}
                    height={height!}
                    src={src!}
                  />
                ),
              }}
            />
            <Edit />
          </>
        )}
      </DocWrapper>
    </div>
  );
};

DocPage.getLayout = function getLayout(page: ReactElement) {
  return <main>{page}</main>;
};

export default DocPage;
