import { Head, Html, Main, NextScript } from "next/document";

const Document = () => (
  <Html>
    <Head>
      <link
        rel="preload"
        href="/site/svg/crystal-blob-light.svg"
        as="image"
        type="image/svg+xml"
      />
      <link
        rel="preload"
        href="/site/svg/crystal-blob.svg"
        as="image"
        type="image/svg+xml"
      />

      <link
        rel="preload"
        href="/site/svg/crystal-logo-white.svg"
        as="image"
        type="image/svg+xml"
      />
      <link
        rel="preload"
        href="/site/svg/crystal-logo.svg"
        as="image"
        type="image/svg+xml"
      />

      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link
        rel="preconnect"
        href="https://fonts.gstatic.com"
        crossOrigin="true"
      />

      <link
        href="https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible:wght@400;700&family=Fira+Code:wght@400;700&display=swap"
        rel="stylesheet"
      />

      <link rel="icon" href="/site/favicons/favicon.ico" />
    </Head>

    <body className="bg-ctp-base">
      <Main />
      <NextScript />
    </body>
  </Html>
);

export default Document;
