/** @type {import('next-seo').DefaultSeoProps} */
export default {
  titleTemplate: "Crystal Linux - %s",
  title: "Official Site",
  defaultTitle: "Crystal Linux",
  description:
    "Crystal Linux is a brand new Arch Linux based distribution. Friendly, powerful, and easy to use.",
  twitter: {
    cardType: "summary",
    handle: "Crystal_Linux",
  },
  openGraph: {
    site_name: "Crystal Linux",
    type: "website",
    images: [
      {
        url: "https://getcryst.al/site/api/og",
        width: 1200,
        height: 630,
        alt: "Og Image Alt",
        type: "image/svg",
      },
    ],
  },
};
